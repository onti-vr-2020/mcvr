﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class LoadController : MonoBehaviour
{
    public string filePath = "Assets/Scenes/Levels/Level1/";
    public GameObject target_model;
    private string json;
    public GameObject standartInteracteble;
    protected Dictionary<string, GameObject> matt_to_resourses= new Dictionary<string,GameObject>();
    public GameObject[] Resourses;
    public string[] matts;
    // Start is called before the first frame update
    void Start()
    {
        for(int i = 0; i < Resourses.Length; i++)
        {
            matt_to_resourses.Add(matts[i], Resourses[i]);
        }
        LoadWorld();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.L))
        {
            //LoadWorld();
            Debug.Log("loading");
        }
    }
    public void LoadWorld()
    {
        Save save = new Save();
        string json =  File.ReadAllText(filePath+"state.json");
        string[] details = JsonUtility.FromJson<Save>(json).details;
        foreach (string res in details)
        {
            Debug.Log(res.Split(':')[1].Length);
            string meshPath = res.Split(':')[0];
            string data = res.Split(':')[1].Substring(1, res.Split(':')[1].Length - 2);
            string[] datas = data.Split(',');
            Vector3 pos = new Vector3();
            Quaternion rot = new Quaternion();
            float x = float.Parse(datas[0].Replace('.', ','));
            float y = float.Parse(datas[1].Replace('.', ','));
            float z = float.Parse(datas[2].Replace('.', ','));
            float x_r = float.Parse(datas[3].Replace('.', ','));
            float y_r = float.Parse(datas[4].Replace('.', ','));
            float z_r = float.Parse(datas[5].Replace('.', ','));
            pos = new Vector3(x, y, z);
            rot.x = x_r;
            rot.y = y_r;
            rot.z = z_r;
            datas[6] = datas[6].Trim('"');
            datas[7] = datas[7].Trim('"');
            FastObjImporter fobj = new FastObjImporter();
            Mesh newMesh; //=// fobj.ImportFile(meshPath);
            var obj = OBJLoader.LoadOBJFile(meshPath);
            newMesh = obj.GetComponentInChildren<MeshFilter>().mesh;
            Destroy(obj);
            Debug.Log(datas[7]);
            GameObject newResourse = Instantiate(matt_to_resourses[datas[7]], pos, rot);
            if (datas[6] != "Stone")
            {
                newResourse.GetComponent<MeshFilter>().mesh = newMesh;
                newResourse.GetComponent<MeshCollider>().sharedMesh = newMesh;
            }
            
            
            GameObject newInteracteble = Instantiate(standartInteracteble, pos, rot);
            newResourse.transform.parent = newInteracteble.transform;
            Resource ResScript = newInteracteble.AddComponent<Resource>();
            ResScript.MaterialType = datas[7] ;
            ResScript.Material = datas[6];
            ResScript.Connected = true;
            newInteracteble.tag = "Resource";
            newResourse.tag = "Resource";

        }
    }
}
