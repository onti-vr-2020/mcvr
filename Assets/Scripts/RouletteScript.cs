﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Valve.VR;
using Valve.VR.InteractionSystem;

public class RouletteScript : MonoBehaviour
{
    public SteamVR_Action_Boolean Touchpad;
    public bool Active = false;
    public GameObject StartPoint, StartPointInPrefab;
    public float Distance;
    public Text text;
    void Update()
    {
        if (Touchpad.state && this.GetComponent<Throwable>().attached)
        {
            Active = true;
        }
        else Active = false;
        if (Active)
        {
            StartPoint.SetActive(true);
            Distance = Mathf.Ceil(Vector3.Distance(StartPointInPrefab.gameObject.transform.position, StartPoint.transform.position) * 100);
        }
        else
        {
            StartPoint.transform.position = StartPointInPrefab.transform.position;
            Distance = 0;
        }
        text.text = Distance.ToString() + "см";
    }
}
