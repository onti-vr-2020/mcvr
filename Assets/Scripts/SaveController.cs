﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System.IO;

public class SaveController : MonoBehaviour
{
    public string filePath = "Assets/Scenes/Levels/Level1/";
    public GameObject target_model;
    private string json;
    // Start is called before the first frame update
    void Start()
    {
        SaveWorld();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.S))
        {
           // SaveWorld();
        }
    }
    public void SaveWorld()
    {
        Save save = new Save();
        float score = GetComponent<VolumeController>().GetScore();
        save.score = score;
        List<GameObject> to_Save = new List<GameObject>();
        GameObject[] objects = GameObject.FindGameObjectsWithTag("Resource");
        int i = 0;
        List<string> details = new List<string>();
        foreach (GameObject go in objects)
        {
            
            if (go.name!= "StandartInteractible(Clone)")
            {
                Vector3 pos = go.transform.position;
                Quaternion rot = go.transform.rotation;
                string path = filePath + i.ToString()+".obj";
                ObjExporter.MeshToFile(go.GetComponent<MeshFilter>(), path);
                string Mat = go.GetComponentInParent<Resource>().Material;
                string MatT = go.GetComponentInParent<Resource>().MaterialType;
                string detail =  path  + ":" + "[" + pos.x.ToString().Replace(',','.') + "," + pos.y.ToString().Replace(',', '.') + "," + pos.z.ToString().Replace(',', '.') + "," + rot.x.ToString().Replace(',', '.') + "," + rot.y.ToString().Replace(',', '.') + "," + rot.z.ToString().Replace(',', '.') +","+Mat+","+MatT+"]";

                details.Add(detail);
                
                i++;
            }
        }
        save.details = details.ToArray();
       
        string json = JsonUtility.ToJson(save);
        StreamWriter writer = new StreamWriter(filePath+"state.json", false);
        writer.Write(json);
        writer.Close();
        //string s = JsonUtility.FromJson<Save>(json).details[0];
        //Debug.Log(s);
    }
}
