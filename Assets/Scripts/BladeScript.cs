﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BladeScript : MonoBehaviour
{
    public string MaterialType;
    public GameObject InBlade()
    {
        List<Collider> cols = new List<Collider>(Physics.OverlapBox(this.gameObject.transform.position, this.transform.localScale, this.transform.rotation));
        for (int i = 0; i< cols.Count; ++i)
        {
            Debug.Log(cols[i].gameObject.tag);
            if (cols[i].gameObject.tag == "Resource")
            {
                if(cols[i].gameObject.GetComponentInParent<Resource>().MaterialType == MaterialType)
                    return cols[i].gameObject;
            }
        }
        return null;
    }
}
