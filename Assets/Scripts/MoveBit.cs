﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveBit : MonoBehaviour
{
    public GameObject BitGO;
    private GameObject FixedObject;
    void Update()
    {
        FixedObject = BitGO.GetComponentInChildren<BladeScript>().InBlade();
        if (FixedObject != null) SetActivatedAnim(true);
    }
    public void SetActivatedAnim(bool value)
    {
        BitGO.GetComponent<Rigidbody>().useGravity = !value;
        BitGO.GetComponent<Rigidbody>().isKinematic = value;
    }
}
