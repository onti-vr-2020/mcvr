﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Simplex3
{
    public Vector3[] vectors;
    public Vector3[] vertices;
    public float positive;
    public float volume;
    public Plane[] planes;
    public Simplex3(Vector3[] vectors)
    {
        this.vectors = vectors;
        this.volume = Vector3.Dot(vectors[0], Vector3.Cross(vectors[1], vectors[2])) / 6.0f;
        this.vertices = new Vector3[] { Vector3.zero, vectors[0], vectors[1], vectors[2] };
        this.positive = Mathf.Sign(volume);
        this.planes = new Plane[] {new Plane(vertices[0], vertices[1], vertices[2]),
                                   new Plane(vertices[0], vertices[1], vertices[3]),
                                   new Plane(vertices[0], vertices[2], vertices[3]),
                                   new Plane(vertices[1], vertices[2], vertices[3])};
    }

    public static float FindIntersectionVolume(Simplex3 s1, Simplex3 s2)
    {
        Vector3 mp;
        List<Vector3> UVertices = new List<Vector3>() {Vector3.zero};
        Simplex3[] s = new Simplex3[] { s1, s2 };
        for (int i = 0; i < 2; i++)
        {
            Vector3[] vectors = s[1 - i].vectors;
            mp = (vectors[0] + vectors[1] + vectors[2]) / 4;
            foreach (Vector3 v in s[i].vectors)
            {
                if ((s[1 - i].planes[0].GetSide(v) == s[1 - i].planes[0].GetSide(mp) || Mathf.Abs(s[1 - i].planes[0].GetDistanceToPoint(v)) < 0.001) &&
                    (s[1 - i].planes[1].GetSide(v) == s[1 - i].planes[1].GetSide(mp) || Mathf.Abs(s[1 - i].planes[1].GetDistanceToPoint(v)) < 0.001) &&
                    (s[1 - i].planes[2].GetSide(v) == s[1 - i].planes[2].GetSide(mp) || Mathf.Abs(s[1 - i].planes[2].GetDistanceToPoint(v)) < 0.001) &&
                    (s[1 - i].planes[3].GetSide(v) == s[1 - i].planes[3].GetSide(mp) || Mathf.Abs(s[1 - i].planes[3].GetDistanceToPoint(v)) < 0.001))
                {
                    if(!SoftContains(UVertices,v))
                        UVertices.Add(new Vector3(v.x, v.y, v.z));
                }
            }
        }
        for (int i = 0; i < 2; i++)
        {
            Vector3[] vectors = s[1 - i].vectors;
            mp = (vectors[0] + vectors[1] + vectors[2]) / 4;

            Vector3[,] edges = new Vector3[6, 2]{{ s[i].vertices[0], s[i].vertices[1]},
                                                 { s[i].vertices[0], s[i].vertices[2]},
                                                 { s[i].vertices[0], s[i].vertices[3]},
                                                 { s[i].vertices[1], s[i].vertices[2]},
                                                 { s[i].vertices[1], s[i].vertices[3]},
                                                 { s[i].vertices[2], s[i].vertices[3]}};
            for(int j = 0; j < 6; j++)
            {
                foreach (Plane p in s[1 - i].planes)
                {
                    
                    if (!(Mathf.Abs(p.GetDistanceToPoint(edges[j, 0])) < 0.001 || Mathf.Abs(p.GetDistanceToPoint(edges[j, 1])) < 0.001))
                    { 


                        float dist;
                        if (p.Raycast(new Ray(edges[j, 0], (edges[j, 1] - edges[j, 0]).normalized), out dist))
                        {
                            if (dist > 0)
                            {
                                if (dist <= (edges[j, 1] - edges[j, 0]).magnitude)
                                {
                                    Vector3 OnPlane = p.ClosestPointOnPlane(edges[j, 0] + (edges[j, 1] - edges[j, 0]).normalized * dist);
                                    if ((s[1 - i].planes[0].GetSide(OnPlane) == s[1 - i].planes[0].GetSide(mp) || Mathf.Abs(s[1 - i].planes[0].GetDistanceToPoint(OnPlane)) < 0.001) &&
                                        (s[1 - i].planes[1].GetSide(OnPlane) == s[1 - i].planes[1].GetSide(mp) || Mathf.Abs(s[1 - i].planes[1].GetDistanceToPoint(OnPlane)) < 0.001) &&
                                        (s[1 - i].planes[2].GetSide(OnPlane) == s[1 - i].planes[2].GetSide(mp) || Mathf.Abs(s[1 - i].planes[2].GetDistanceToPoint(OnPlane)) < 0.001) &&
                                        (s[1 - i].planes[3].GetSide(OnPlane) == s[1 - i].planes[3].GetSide(mp) || Mathf.Abs(s[1 - i].planes[3].GetDistanceToPoint(OnPlane)) < 0.001))
                                    {
                                        if (!SoftContains(UVertices, OnPlane))
                                            UVertices.Add(new Vector3(OnPlane.x, OnPlane.y, OnPlane.z));
                                    }
                                }
                            }
                        }
                        
                    }
                }
            }
        }


        List<Vector3>[] faces = new List<Vector3>[8];
        for(int i = 0; i < 8; i++)
        {
            faces[i] = new List<Vector3>();
            Plane p = s[1 - i / 4].planes[i % 4];
            foreach(Vector3 v in UVertices)
            {
                if(Mathf.Abs(p.GetDistanceToPoint(v)) < 0.001)
                {
                    faces[i].Add(v);
                }
            }

            mp = Vector3.zero;
            foreach (Vector3 v in faces[i])
                mp += v;
            mp /= faces[i].Count;
            if (faces[i].Count >= 3)
            {
                RotComparer comp = new RotComparer(new Plane(Vector3.Cross(p.normal, p.ClosestPointOnPlane(Vector3.one) - mp).normalized, Vector3.zero), mp, p.ClosestPointOnPlane(Vector3.one) - mp);
                faces[i].Sort(comp);
                for(int j = 0; j < faces[i].Count; j++)
                {
                    Debug.DrawLine(faces[i][j], faces[i][(j+1)% faces[i].Count], Color.red);
                }
            }
            
        }

        mp = Vector3.zero;
        foreach (Vector3 v in UVertices)
            mp += v;
        mp /= UVertices.Count;
        float IVolume = 0;
        for (int i = 0; i < 8; i++)
        {
            if (faces[i].Count >= 3)
            {
                int[,] ids = TSplitID(faces[i].Count);
                for (int j = 0; j < faces[i].Count - 2; j++)
                {
                    IVolume += Mathf.Abs(Vector3.Dot(faces[i][ids[j, 0]] - mp, Vector3.Cross(faces[i][ids[j, 1]] - mp, faces[i][ids[j, 2]] - mp)) / 6.0f);
                }
            }
        }
        return IVolume;
    }

    private static int[,] TSplitID(int n)
    {
        int[,] ids = new int[n - 2, 3];
        for(int i =0; i < n - 2; i++)
        {
            ids[i, 0] = 0;
            ids[i, 1] = 1 + i;
            ids[i, 2] = 2 + i;
        }
        return ids;
    }

    public static bool SoftContains(List<Vector3> list, Vector3 v)
    {
        bool contains = false;
        float delta = 0.001f;
        foreach(Vector3  vi in list)
        {
            if((Mathf.Abs(vi.x - v.x) < delta) &&
               (Mathf.Abs(vi.y - v.y) < delta) &&
               (Mathf.Abs(vi.z - v.z) < delta))
            {
                contains = true;
                break;
            }
        }
        return contains;
    }


    public class RotComparer : IComparer<Vector3>
    {
        Plane MainPlane, SplitPlane;
        Vector3 up, mp;
        public RotComparer(Plane _SplitPlane, Vector3 _mp, Vector3 _up)
        {
            SplitPlane = _SplitPlane;
            mp = _mp;
            up = _up;
            
        }
        public int Compare(Vector3 v1, Vector3 v2)
        {
            float angle1 = Vector3.Angle(up, v1 - mp);
            if (SplitPlane.GetSide(v1 - mp))
               angle1 = 360 - angle1;
            float angle2 = Vector3.Angle(up, v2 - mp);
            if (SplitPlane.GetSide(v2 - mp) )
                angle2 = 360 - angle2;


            if (angle1 > angle2)
                return 1;
            else
                return -1;
        }
    }
}
