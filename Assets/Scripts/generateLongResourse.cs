﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class generateLongResourse : MonoBehaviour
{
    public Transform spawnPoint;
    public GameObject resourse;
    public GameObject standartInteracteble;
    public float scaleFactor = 0.1f;
    public bool isGenerating = true;
    public string Material, MaterialType;
    protected GameObject longResourse;
    private Vector3 v3OrgPos;
    private float orgScale;
    private float endScale;
    private float min_z;
    private List<int> vids;

    void Start()
    {
       
        orgScale = spawnPoint.localScale.z;
        endScale = orgScale;
        Vector3[] vertecies = resourse.GetComponent<MeshFilter>().sharedMesh.vertices;
        float max_z = -Mathf.Infinity;
        min_z = Mathf.Infinity;
        for (int i = 0; i < vertecies.Length; i++)
        {
            if(vertecies[i].z > max_z)
            {
                max_z = vertecies[i].z;
            }
            if (vertecies[i].z < min_z)
            {
                min_z = vertecies[i].z;
            }
        }
        vids = new List<int>();
        for (int i = 0; i < vertecies.Length; i++)
        {
            if (vertecies[i].z == max_z)
            {
                vids.Add(i);
            }
        }

    }

    public void isProduce()
    {
        if (isGenerating)
        {
            isGenerating = false;
        }
        else
        {
            isGenerating = true;
        }
    }
    // Update is called once per frame
    void FixedUpdate()
    {
        if (isGenerating) {
            if (longResourse == null)
            {
                longResourse = Instantiate(resourse, spawnPoint.position, spawnPoint.rotation);
                longResourse.transform.localScale = spawnPoint.localScale;
                longResourse.transform.position = spawnPoint.position;
                v3OrgPos = longResourse.transform.position ;
                Vector3[] vertecies = longResourse.GetComponentInChildren<MeshFilter>().mesh.vertices;
                foreach (int ids in vids)
                {
                    vertecies[ids].z = min_z;
                }
                longResourse.GetComponentInChildren<MeshFilter>().mesh.vertices = vertecies;
                longResourse.GetComponent<MeshCollider>().sharedMesh = longResourse.GetComponent<MeshFilter>().mesh;
            }
            else {
                longResourse.transform.position = spawnPoint.position;
                ResizeOn(scaleFactor);
            }
            
        }
        else
        {
            if  (longResourse != null)
                {
                GameObject newInteracteble = Instantiate(standartInteracteble, longResourse.transform.position, longResourse.transform.rotation);
                GameObject newResourse = Instantiate(resourse, longResourse.transform.position, longResourse.transform.rotation);
                newResourse.transform.parent = newInteracteble.transform;
                newResourse.GetComponent<MeshFilter>().mesh = longResourse.GetComponent<MeshFilter>().mesh;
                newResourse.GetComponent<MeshCollider>().sharedMesh = longResourse.GetComponent<MeshFilter>().mesh;
                Resource ResScript = newInteracteble.AddComponent<Resource>();
                ResScript.MaterialType = MaterialType;
                ResScript.Material = Material;
                newInteracteble.tag = "Resource";
                newResourse.tag = "Resource";
                Destroy(longResourse);
            }
        }
            
    }
    private void ResizeOn(float speed)
    {
        Vector3[] vertecies = longResourse.GetComponentInChildren<MeshFilter>().mesh.vertices;
        foreach (int ids in vids)
        {
            vertecies[ids] += new Vector3(0, 0, 0.001f);
        }
        longResourse.GetComponentInChildren<MeshFilter>().mesh.vertices = vertecies;
        longResourse.GetComponent<MeshCollider>().sharedMesh = longResourse.GetComponent<MeshFilter>().mesh;
    }
}
