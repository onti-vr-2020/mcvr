﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class generateResourse : MonoBehaviour
{
    public Transform spawnPoint;
    public GameObject resourse;
    public GameObject standartInteracteble;
    public string MaterialType, Material;
    public float distance = 1f;
    protected GameObject newResourse;
    protected GameObject newInteracteble;
    // Start is called before the first frame update
    void Start()
    {
        newInteracteble = Instantiate(standartInteracteble, spawnPoint.position, spawnPoint.rotation);
        newResourse = Instantiate(resourse, spawnPoint.position, spawnPoint.rotation);
        newResourse.transform.parent = newInteracteble.transform;
        Resource ResScript = newInteracteble.AddComponent<Resource>();
        ResScript.MaterialType = MaterialType;
        ResScript.Material = Material;
        newInteracteble.tag = "Resource";

        newResourse.tag = "Resource";
    }

    // Update is called once per frame
    void Update()
    {
        if (Vector3.Distance(newResourse.transform.position, spawnPoint.position)>= distance)
        {
            newInteracteble = Instantiate(standartInteracteble, spawnPoint.position, spawnPoint.rotation);
            newResourse = Instantiate(resourse, spawnPoint.position, spawnPoint.rotation);
            newResourse.transform.parent = newInteracteble.transform;
            Resource ResScript = newInteracteble.AddComponent<Resource>();
            ResScript.MaterialType = MaterialType;
            ResScript.Material = Material;
            newInteracteble.tag = "Resource";
            
            newResourse.tag = "Resource";
        }
    }
}
