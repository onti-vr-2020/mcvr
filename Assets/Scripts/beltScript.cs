﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;
public class beltScript : MonoBehaviour
{
    public GameObject[] PointsForTools;
    public string[] tags;
    void CheckToolsInPoint(string tag, GameObject Point)
    {
        List<Collider> cols = new List<Collider>(Physics.OverlapBox(Point.gameObject.transform.position, Point.gameObject.transform.localScale, Point.gameObject.transform.rotation));
        for (int i = 0; i < cols.Count; ++i)
        {
            if(cols[i].gameObject.tag == tag)
            {
                if (cols[i].GetComponent<Throwable>().attached) cols[i].gameObject.transform.position = Point.transform.position;
            }
        }
    }
}
