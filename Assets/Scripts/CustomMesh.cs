﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomMesh
{
    public List<Vector3> vertices;
    public List<Vector3> normals;
    public List<Vector2> uvs;
    public List<int> triangles;

    public CustomMesh()
    {
        this.vertices = new List<Vector3>();
        this.normals = new List<Vector3>();
        this.uvs = new List<Vector2>();
        this.triangles = new List<int>();
    }

    public CustomMesh(List<Vector3> _verticies, List<Vector3> _normals, List<Vector2> _uvs, List<int> _triangles)
    {
        this.vertices = _verticies;
        this.normals = _normals;
        this.uvs = _uvs;
        this.triangles = _triangles;
    }

    public CustomMesh(Mesh _mesh)
    {
        vertices = new List<Vector3>();
        foreach (Vector3 v in _mesh.vertices)
            vertices.Add(v);
        normals = new List<Vector3>();
        foreach (Vector3 n in _mesh.normals)
            normals.Add(n);
        uvs = new List<Vector2>();
        foreach (Vector3 uvi in _mesh.uv)
            uvs.Add(uvi);
        triangles = new List<int>();
        foreach (int i in _mesh.triangles)
            triangles.Add(i);
    }

    public Mesh GetMesh()
    {
        Mesh mesh = new Mesh();
        mesh.SetVertices(vertices);
        mesh.SetNormals(normals);
        mesh.SetUVs(0, uvs);
        mesh.SetUVs(1, uvs);
        mesh.SetTriangles(triangles, 0);
        return mesh;
    }

    public void AddTriangle(List<Vector3> _verticies, List<Vector3> _normals, List<Vector2> _uvs)
    {
        if (Vector3.Dot(Vector3.Cross(_verticies[1] - _verticies[0], _verticies[2] - _verticies[0]), _normals[0]) < 0)
        {
            _verticies.Reverse();
            _normals.Reverse();
            _uvs.Reverse();
        }

        vertices.AddRange(_verticies);
        normals.AddRange(_normals);
        uvs.AddRange(_uvs);

        for (int i = 0; i < 3; i++)
        {
            triangles.Add(triangles.Count);
        }

    }


}
