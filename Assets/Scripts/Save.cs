﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[Serializable]
public class Save
{

    // Start is called before the first frame update
    public string target_model = "target_model.fbx";
    public string[] details;
    public float score = 0f;
    public Save()
    {
         details = new string[1]; 
    }
}
