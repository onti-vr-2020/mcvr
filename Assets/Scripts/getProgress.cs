﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Valve.VR.InteractionSystem
{
    public class getProgress : MonoBehaviour
    {
        public int index;
        // Start is called before the first frame update
        private float progress;
        void Start()
        {
            index = GetComponent<TeleportPoint>().sceneIndex;
            progress = getProgressFromFile("Assets/Scenes/Levels/Level"+index.ToString()+"/");
            GetComponent<TeleportPoint>().title += " " + progress * 100 + "%";
        }

        // Update is called once per frame
        void Update()
        {

        }

        float getProgressFromFile(string file)
        {
            return JsonUtility.FromJson<Save>(file).score;
        }
    }
}