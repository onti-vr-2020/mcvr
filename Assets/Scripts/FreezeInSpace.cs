﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;

public class FreezeInSpace : MonoBehaviour
{
    public SteamVR_Action_Boolean Squeeze;
    public bool Grip;
    private RaycastHit objectHit;

    // Start is called before the first frame update
    void Start()
    {
        
    }
    private void invertKinematic(Rigidbody rigid)
    {
        if (rigid.isKinematic)
            rigid.isKinematic = false;
        else
            rigid.isKinematic = true;
    }
    // Update is called once per frame
    void Update()
    {
        Grip = Squeeze.state;
        if (Grip)
        {
            Vector3 fwd = transform.TransformDirection(Vector3.forward);
            
            if (Physics.Raycast(transform.position, fwd, out objectHit, 1))
            {
                //do something if hit object ie
                if (objectHit.collider.gameObject.layer == 9)
                {
                    GameObject g = objectHit.collider.gameObject;
                    if (g.GetComponent<Rigidbody>())
                    {
                        invertKinematic(g.GetComponent<Rigidbody>());
                    }
                    else if (g.GetComponentInParent<Rigidbody>()) {
                        invertKinematic(g.GetComponentInParent<Rigidbody>());
                    }
                    else if (g.transform.parent.GetComponentInParent<Rigidbody>())
                    {
                        invertKinematic(g.transform.parent.GetComponentInParent<Rigidbody>());
                    }
                }
            }
        }
    }
}
   

