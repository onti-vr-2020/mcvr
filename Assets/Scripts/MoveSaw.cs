﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;

public class MoveSaw : MonoBehaviour
{
    public bool Fixed = false;
    public string state = "forward";
    public GameObject SawGO, StartGO, EndGO, StartInPrefab, EndInPrefab, FixedObject, Blade;
    private Slicer slicer;
    private bool sleep;
    public float sleepTime = 1f;
    private float startSleep;
    private LinearDrive ld;

    private void Start()
    {
        slicer = GameObject.Find("SlicerManager").GetComponent<Slicer>();
        ld = SawGO.GetComponent<LinearDrive>();
    }

    void Update()
    {
        if (SawGO.GetComponent<Throwable>().attached)
        {
            if (!sleep)
            {
                if (SawGO.GetComponentInChildren<BladeScript>().InBlade() && !Fixed)
                {
                    Fixed = true;
                    SetActivatedAnim(true);
                    FixedObject = SawGO.GetComponentInChildren<BladeScript>().InBlade();
                    FixedObject.GetComponentInParent<Rigidbody>().isKinematic = true;
                    state = "forward";
                }
                if (Fixed)
                {
                    if (state == "forward")
                    {
                        if (SawGO.transform.position == EndGO.transform.position)
                        {
                            state = "backward";
                        }
                    }
                    else if (state == "backward")
                    {
                        if (SawGO.transform.position == StartGO.transform.position)
                        {
                            state = "forward";
                            SetActivatedAnim(false);
                            SawGO.transform.position -= Blade.transform.up * 0.05f;
                            SetActivatedAnim(true);

                            if (SawGO.GetComponentInChildren<BladeScript>().InBlade() != FixedObject)
                            {
                                FixedObject.GetComponentInParent<Rigidbody>().isKinematic = false;
                                slicer.Cut(FixedObject, Blade.transform.position, Blade.transform.forward);
                                Fixed = false;
                                FixedObject = null;
                                SetActivatedAnim(false);
                                startSleep = Time.time;
                                sleep = true;
                            }
                        }
                    }
                    Debug.Log(state);
                }
            }
            else if (Time.time - startSleep >= sleepTime)
            {
                sleep = false;
                startSleep = 0;

            }
        }
    }
    private void OnDrawGizmos()
    {
        Gizmos.DrawRay(new Ray(Blade.transform.position, Blade.transform.forward));
    }
    public void SetActivatedAnim(bool value)
    {
        ld.Active = value;
        StartGO.transform.position = StartInPrefab.transform.position;
        EndGO.transform.position = EndInPrefab.transform.position;
        SawGO.GetComponent<Rigidbody>().useGravity = !value;
        SawGO.GetComponent<Rigidbody>().isKinematic = value;
    }
}
