﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;
public class MoveScissors : MonoBehaviour
{
    public bool Fixed = false;
    public GameObject ScissorsGO, StartGO, EndGO, StartInPrefab, EndInPrefab, FixedObject, Blade;
    private Slicer slicer;
    private bool sleep;
    public float sleepTime = 1f;
    private float startSleep;
    private LinearDrive ld;

    private void Start()
    {
        slicer = GameObject.Find("SlicerManager").GetComponent<Slicer>();
        ld = ScissorsGO.GetComponent<LinearDrive>();
    }

    void Update()
    {
        if (ScissorsGO.GetComponent<Throwable>().attached)
        {
            if (!sleep)
            {
                if (ScissorsGO.GetComponentInChildren<BladeScript>().InBlade() && !Fixed)
                {
                    Fixed = true;
                    FixedObject = ScissorsGO.GetComponentInChildren<BladeScript>().InBlade();
                    SetActivatedAnim(true);
                    FixedObject.GetComponentInParent<Rigidbody>().isKinematic = true;
                }
                if (Fixed)
                {

                    if (ScissorsGO.GetComponentInChildren<BladeScript>().InBlade() != FixedObject)
                    {
                        FixedObject.GetComponentInParent<Rigidbody>().isKinematic = false;
                        slicer.Cut(FixedObject, Blade.transform.position, Blade.transform.forward);
                        Fixed = false;
                        FixedObject = null;
                        SetActivatedAnim(false);
                        startSleep = Time.time;
                        sleep = true;
                    }
                }                
            }
            else if (Time.time - startSleep >= sleepTime)
            {
                sleep = false;
                startSleep = 0;

            }
        }
    }
    public void SetActivatedAnim(bool value)
    {
        //if (value) this.transform.rotation = FixedObject.transform.rotation;
        if (value)
        {
            ScissorsGO.transform.Rotate(Quaternion.FromToRotation(ScissorsGO.transform.TransformDirection(Vector3.forward), FixedObject.transform.TransformDirection(Vector3.up)).ToEulerAngles());
        }
        ld.Active = value;
        StartGO.transform.position = StartInPrefab.transform.position;
        EndGO.transform.position = EndInPrefab.transform.position;
        ScissorsGO.GetComponent<Rigidbody>().useGravity = !value;
        ScissorsGO.GetComponent<Rigidbody>().isKinematic = value;
    }
}
