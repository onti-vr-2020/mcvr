﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Resource : MonoBehaviour
{
    public string MaterialType, Material;
    public bool Connected;
    public GameObject hand;

    public void Start()
    {

    }

    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.J))
            Connect();
    }

    public void Connect()
    {
        if (!Connected)
        {
            Connected = true;
            Vector3 pos = gameObject.transform.position;
            pos.x -= pos.x % 0.01f;
            pos.y -= pos.y % 0.01f;
            pos.z -= pos.z % 0.01f;
            gameObject.transform.position = pos;
            gameObject.GetComponent<Rigidbody>().useGravity = false;
            gameObject.GetComponent<Rigidbody>().isKinematic = true;
        }
        else
        {
            Connected = false;
            gameObject.GetComponent<Rigidbody>().useGravity = true;
            gameObject.GetComponent<Rigidbody>().isKinematic = false;
        }
    }
}
