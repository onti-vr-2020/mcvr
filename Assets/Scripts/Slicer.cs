﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Slicer : MonoBehaviour
{
    public GameObject Interactable;
    Plane plane;

    public void Cut(GameObject ObjToSplit, Vector3 center, Vector3 normal)
    {
        plane = new Plane(ObjToSplit.transform.InverseTransformDirection(normal), ObjToSplit.transform.InverseTransformPoint(center));

        Mesh MeshToSplit = ObjToSplit.GetComponent<MeshFilter>().mesh;
        CustomMesh LeftGenMesh = new CustomMesh();
        CustomMesh RightGenMesh = new CustomMesh();
        List<int> Intersections = new List<int>();
        List<List<int>> VertexIds = new List<List<int>>();
        List<CustomMesh> CustomMeshes = new List<CustomMesh>();
        List<Vector3[]> IntersectionPairsVertecies = new List<Vector3[]>();
        List<Vector3[]> IntersectionPairsNormals = new List<Vector3[]>();
        List<Vector2[]> IntersectionPairsUVs = new List<Vector2[]>();
        
        for (int i = 0; i < MeshToSplit.triangles.Length; i += 3)
        {
            bool[] isLeft = new bool[3];
            int[] TriangleIndexes = new int[3] { MeshToSplit.triangles[i], MeshToSplit.triangles[i + 1], MeshToSplit.triangles[i + 2] };
            isLeft[0] = plane.GetSide(MeshToSplit.vertices[TriangleIndexes[0]]);
            isLeft[1] = plane.GetSide(MeshToSplit.vertices[TriangleIndexes[1]]);
            isLeft[2] = plane.GetSide(MeshToSplit.vertices[TriangleIndexes[2]]);

            if (isLeft[0] && isLeft[1] && isLeft[2])
            {
                LeftGenMesh.AddTriangle(new List<Vector3>() { MeshToSplit.vertices[TriangleIndexes[0]],
                                                              MeshToSplit.vertices[TriangleIndexes[1]],
                                                              MeshToSplit.vertices[TriangleIndexes[2]]},
                                        new List<Vector3>() { MeshToSplit.normals[TriangleIndexes[0]],
                                                              MeshToSplit.normals[TriangleIndexes[1]],
                                                              MeshToSplit.normals[TriangleIndexes[2]]},
                                        new List<Vector2>() { MeshToSplit.uv[TriangleIndexes[0]],
                                                              MeshToSplit.uv[TriangleIndexes[1]],
                                                              MeshToSplit.uv[TriangleIndexes[2]]});
            }
            else if (!isLeft[0] && !isLeft[1] && !isLeft[2])
            {
                RightGenMesh.AddTriangle(new List<Vector3>() { MeshToSplit.vertices[TriangleIndexes[0]],
                                                              MeshToSplit.vertices[TriangleIndexes[1]],
                                                              MeshToSplit.vertices[TriangleIndexes[2]]},
                                        new List<Vector3>() { MeshToSplit.normals[TriangleIndexes[0]],
                                                              MeshToSplit.normals[TriangleIndexes[1]],
                                                              MeshToSplit.normals[TriangleIndexes[2]]},
                                        new List<Vector2>() { MeshToSplit.uv[TriangleIndexes[0]],
                                                              MeshToSplit.uv[TriangleIndexes[1]],
                                                              MeshToSplit.uv[TriangleIndexes[2]]});
            }
            else
            {
                List<int> LeftVerticies = new List<int>();
                List<int> RightVerticies = new List<int>();

                for (int j = 0; j < 3; j++)
                {
                    if (isLeft[j])
                        LeftVerticies.Add(TriangleIndexes[j]);
                    else
                        RightVerticies.Add(TriangleIndexes[j]);
                }
                
                Vector3[] IntersectionsVerticies = new Vector3[2];
                Vector3[] IntersectionNormals = new Vector3[2];
                Vector2[] IntersectionsUVs = new Vector2[2];

                VertexIds.Clear();
                CustomMeshes.Clear();
                VertexIds.Add(LeftVerticies);
                VertexIds.Add(RightVerticies);
                CustomMeshes.Add(LeftGenMesh);
                CustomMeshes.Add(RightGenMesh);
                if (RightVerticies.Count == 2)
                {
                    VertexIds.Reverse();
                    CustomMeshes.Reverse();
                }

                float dist;
                float normDist;
                for (int k = 0; k < 2; k++)
                {
                    plane.Raycast(new Ray(MeshToSplit.vertices[VertexIds[0][k]],
                        (MeshToSplit.vertices[VertexIds[1][0]] - MeshToSplit.vertices[VertexIds[0][k]]).normalized),
                        out dist);
                    IntersectionsVerticies[k] = MeshToSplit.vertices[VertexIds[0][k]] +
                        (MeshToSplit.vertices[VertexIds[1][0]] - MeshToSplit.vertices[VertexIds[0][k]]).normalized * dist;
                    normDist = dist / (MeshToSplit.vertices[VertexIds[1][0]] - MeshToSplit.vertices[VertexIds[0][k]]).magnitude;
                    IntersectionNormals[k] = Vector3.Lerp(MeshToSplit.normals[VertexIds[0][k]], MeshToSplit.normals[VertexIds[1][0]], normDist);
                    IntersectionsUVs[k] = Vector2.Lerp(MeshToSplit.uv[VertexIds[0][k]], MeshToSplit.uv[VertexIds[1][0]], normDist);
                }


                CustomMeshes[0].AddTriangle(new List<Vector3>() { MeshToSplit.vertices[VertexIds[0][0]],
                                                            IntersectionsVerticies[0],
                                                            IntersectionsVerticies[1]},
                                        new List<Vector3>() { MeshToSplit.normals[VertexIds[0][0]],
                                                            IntersectionNormals[0],
                                                            IntersectionNormals[1]},
                                        new List<Vector2>() { MeshToSplit.uv[VertexIds[0][0]],
                                                            IntersectionsUVs[0],
                                                            IntersectionsUVs[1]});
                int closestIntersection = 1;

                CustomMeshes[0].AddTriangle(new List<Vector3>() { MeshToSplit.vertices[VertexIds[0][0]],
                                                            MeshToSplit.vertices[VertexIds[0][1]],
                                                            IntersectionsVerticies[closestIntersection]},
                                        new List<Vector3>() { MeshToSplit.normals[VertexIds[0][0]],
                                                            MeshToSplit.normals[VertexIds[0][1]],
                                                            IntersectionNormals[closestIntersection]},
                                        new List<Vector2>() { MeshToSplit.uv[VertexIds[0][0]],
                                                            MeshToSplit.uv[VertexIds[0][1]],
                                                            IntersectionsUVs[closestIntersection]});
                
                CustomMeshes[1].AddTriangle(new List<Vector3>() { MeshToSplit.vertices[VertexIds[1][0]],
                                                            IntersectionsVerticies[0],
                                                            IntersectionsVerticies[1]},
                                        new List<Vector3>() { MeshToSplit.normals[VertexIds[1][0]],
                                                            IntersectionNormals[0],
                                                            IntersectionNormals[1]},
                                        new List<Vector2>() { MeshToSplit.uv[VertexIds[1][0]],
                                                            IntersectionsUVs[0],
                                                            IntersectionsUVs[1]});

                IntersectionPairsVertecies.Add(IntersectionsVerticies);
                IntersectionPairsNormals.Add(IntersectionNormals);
                IntersectionPairsUVs.Add(IntersectionsUVs);
            }
        }
        Vector3 mp = Vector3.zero;
        for (int i = 0; i < IntersectionPairsVertecies.Count; i++)
            mp += IntersectionPairsVertecies[i][1];
        mp /= IntersectionPairsVertecies.Count;

        for (int i = 0; i < IntersectionPairsVertecies.Count; i++)
        {

            LeftGenMesh.AddTriangle(new List<Vector3>() {IntersectionPairsVertecies[i][1],
                                                        IntersectionPairsVertecies[i][0],
                                                        mp},
                                        new List<Vector3>() {-plane.normal,
                                                        -plane.normal,
                                                        -plane.normal },
                                        new List<Vector2>() {IntersectionPairsUVs[i][1],
                                                        IntersectionPairsUVs[i][0],
                                                        new Vector2(0.5f, 0.5f)});

            RightGenMesh.AddTriangle(new List<Vector3>() {IntersectionPairsVertecies[i][1],
                                                        IntersectionPairsVertecies[i][0],
                                                        mp,},
                                        new List<Vector3>() {plane.normal,
                                                        plane.normal,
                                                        plane.normal},
                                        new List<Vector2>() {IntersectionPairsUVs[i][1],
                                                        IntersectionPairsUVs[i][0],
                                                        new Vector2(0.5f, 0.5f)});

        }
        /*
        Vector3 LeftMp = Vector3.zero;
        for(int i = 0; i < LeftGenMesh.verticies.Count; i++)
        {
            LeftMp += LeftGenMesh.verticies[i];
        }
        LeftMp /= LeftGenMesh.verticies.Count;
        for (int i = 0; i < LeftGenMesh.verticies.Count; i++)
        {
            LeftGenMesh.verticies[i] -= LeftMp;
        }

        Vector3  RightMp = Vector3.zero;
        for (int i = 0; i < RightGenMesh.verticies.Count; i++)
        {
            RightMp += RightGenMesh.verticies[i];
        }
        RightMp /= RightGenMesh.verticies.Count;
        for (int i = 0; i < RightGenMesh.verticies.Count; i++)
        {
            RightGenMesh.verticies[i] -= RightMp;
        }   
        */
        ObjToSplit.GetComponent<MeshFilter>().mesh = LeftGenMesh.GetMesh();
        ObjToSplit.GetComponent<MeshCollider>().sharedMesh = LeftGenMesh.GetMesh();
        ObjToSplit.GetComponent<MeshCollider>().convex = true;


        GameObject RightObject = new GameObject();
        RightObject.AddComponent<MeshFilter>().mesh = RightGenMesh.GetMesh();
        RightObject.AddComponent<MeshCollider>().sharedMesh = RightGenMesh.GetMesh();
        RightObject.GetComponent<MeshCollider>().convex = true;
        RightObject.AddComponent<MeshRenderer>().materials = ObjToSplit.GetComponent<MeshRenderer>().materials;
        RightObject.tag = "Resource";
        RightObject.transform.position = ObjToSplit.transform.position;
        RightObject.transform.localScale = ObjToSplit.transform.localScale;
        RightObject.transform.rotation = ObjToSplit.transform.rotation;


        GameObject RightInteractable = Instantiate(Interactable);
        RightInteractable.transform.position = ObjToSplit.transform.parent.position;
        RightInteractable.transform.localScale = ObjToSplit.transform.parent.localScale;
        RightInteractable.transform.rotation = ObjToSplit.transform.parent.rotation;
        RightInteractable.tag = "Resource";
        Resource res = RightInteractable.AddComponent<Resource>();
        res.Material = ObjToSplit.GetComponentInParent<Resource>().Material;
        res.MaterialType = ObjToSplit.GetComponentInParent<Resource>().MaterialType;
        RightObject.transform.parent = RightInteractable.transform;
        

    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawRay(new Ray(gameObject.transform.TransformDirection(plane.ClosestPointOnPlane(gameObject.transform.position)), gameObject.transform.TransformDirection(plane.normal)));
    }
}
