﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PanicleScript : MonoBehaviour
{
    public GameObject Nozzle;
    private void Update()
    {
        List<Collider> cols = new List<Collider>(Physics.OverlapBox(Nozzle.gameObject.transform.position, Nozzle.transform.localScale, Nozzle.transform.rotation));
        for (int i = 0; i < cols.Count; ++i)
        {
            if (cols[i].gameObject.tag == "Resource")
            {
                Destroy(cols[i].gameObject.transform.parent.gameObject);
            }
        }
    }
}