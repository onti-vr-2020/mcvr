﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class VolumeController : MonoBehaviour
{
    private float WFVolume, BVolume, IVolume;
    private List<Simplex3> WFSimplex3s, BSimplex3s;

    private void Start()
    {
        WFSimplex3s = new List<Simplex3>();
        BSimplex3s = new List<Simplex3>();
    }

    private void CalculateVolumes()
    {
        WFSimplex3s.Clear();
        BSimplex3s.Clear();
        foreach(GameObject res in GameObject.FindGameObjectsWithTag("Resource"))
        {
            if (res.GetComponent<Resource>())
            {
                if (res.GetComponent<Resource>().Connected)
                    BSimplex3s.AddRange(SplitToSimplex3(res));
            }
        }
        WFSimplex3s = SplitToSimplex3(GameObject.FindGameObjectWithTag("WireFrame"));

        BVolume = 0;
        foreach (Simplex3 bs3 in BSimplex3s)
            BVolume += bs3.volume;
        BVolume = Mathf.Abs(BVolume);

        WFVolume = 0;
        foreach (Simplex3 wfs3 in WFSimplex3s)
            WFVolume += wfs3.volume;
        WFVolume = Mathf.Abs(WFVolume);

        IVolume = 0;
        foreach (Simplex3 bs3 in BSimplex3s)
        {
            foreach(Simplex3 wfs3 in WFSimplex3s)
            {
                IVolume += bs3.positive* wfs3.positive * Simplex3.FindIntersectionVolume(bs3, wfs3);
            }
        }

        IVolume = Mathf.Min(IVolume, BVolume);
    }

    public float GetScore()
    {
        CalculateVolumes();
        return Mathf.Min(1, Mathf.Max(0, IVolume / (WFVolume + BVolume - IVolume)));
    }

    public List<Simplex3> SplitToSimplex3(GameObject obj)
    {
        List<Simplex3> splited = new List<Simplex3>();
        Mesh mesh = obj.GetComponentInChildren<MeshFilter>().mesh;
        Vector3 mp = Vector3.zero;
        foreach(Vector3 v in mesh.vertices)
        {
            mp += v;
        }
        mp /= mesh.vertices.Length;
        for(int i = 0; i < mesh.triangles.Length; i += 3)
        {
            splited.Add(new Simplex3(new Vector3[] { obj.transform.TransformPoint(mesh.vertices[mesh.triangles[i]]),
                                                     obj.transform.TransformPoint(mesh.vertices[mesh.triangles[i + 1]]),
                                                     obj.transform.TransformPoint(mesh.vertices[mesh.triangles[i + 2]])}));
        }
        return splited;
    }
}
